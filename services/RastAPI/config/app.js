const express = require('express'),
      app = express(),
      bodyParser = require('body-parser'),
      morgan = require('morgan'),
      consign = require('consign'),
      cors = require('cors');

app.use(express.static('.'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors());

consign({ cwd: 'services' })
  .then('RastAPI/app/api')
  .then('RastAPI/app/routes')
  .into(app);

module.exports = app;