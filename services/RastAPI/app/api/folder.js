const api = {};
const fs = require('fs-extra');
const path = require('path');
const homedir = require('os').homedir();

api.getFolder = (req, res) => {
  res.send( getDirectory(req.body.path) );
}

api.copyItems = (req, res) => {
  let info = req.body;
  for (let i = 0; i < info.items.length; i++) {
    try {
      fs.copy(info.items[i].path, info.to + '/' + info.items[i].name);
    } catch (err) {
      console.error(err)
    }
  }
}

api.moveItems = (req, res) => {
  let info = req.body;
  for (let i = 0; i < info.items.length; i++) {
    try {
      fs.move(info.items[i].path, info.to + '/' + info.items[i].name);
    } catch (err) {
      console.error(err);
    }
  }
}

api.deleteItems = (req, res) => {
  let info = req.body;
  for (let i = 0; i < info.items.length; i++) {
    try {
      fs.remove(info.items[i].path);
    } catch (err) {
      console.error(err);
    }
  }
}

module.exports = api;

function getDirectory(directoryPath) {
  let directory = {
    active: true,
    path: directoryPath ? directoryPath.replace('//','/') : homedir,
    pathItems: [],
    items: []
  };
  directory.pathItems = formatPath(directory.path);
  let list = fs.readdirSync(directory.path).filter(name => (!name.startsWith('.') && !name.startsWith('$') && !name.startsWith('~')));
  for (let i = 0; i < list.length; i++) {
    let fileInfo = fs.statSync(directory.path + '/' + list[i]);
    directory.items.push({
      name: list[i],
      path: directory.path + '/' + list[i],
      size: {
        text: (fileInfo.isDirectory() ? '--' : formatBytes(fileInfo['size']).toString()),
        raw: (fileInfo.isDirectory() ? 0 : fileInfo['size'])
      },
      modified: formatDatetime(fileInfo['mtime']),
      created: formatDatetime(fileInfo['ctime']),
      type: (fileInfo.isFile() ? getFiletype(list[i]) : 'Папка')
    });
  }
  return directory;
}

function getParentDirectory(directoryPath) {
  return getDirectory( path.resolve(directoryPath, '..') );
}

function formatPath(dirPath) {
  let items = [];
  let itemsNames = dirPath.split('/');
  for (let i = 0; i < itemsNames.length; i++) {
    items.push({
      name: itemsNames[i],
      path: dirPath.split(itemsNames[i])[0] + itemsNames[i]
    });
  }
  return items;
}

function formatBytes(bytes, decimals = 2) {
  if (bytes === 0) return '0 Б';
  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Б', 'КБ', 'МБ', 'ГБ', 'ТБ', 'ПБ', 'ЭБ', 'ЗБ', 'ИБ'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function formatDatetime(date) {
  let dateObj = new Date(date);
  let options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric'
  }
  return dateObj.toLocaleString('ru-RU', options);
}

function getFiletype(filename) {
  switch (path.extname(filename)) {
    case '.jpg':
    case '.jpeg':
    case '.png':
    case '.tiff':
    case '.gif':
    case '.heic':
    case '.bpm':
      return 'Изображение';
    case '.avi':
    case '.mkv':
    case '.mov':
    case '.mpeg':
    case '.mp4':
    case '.flv':
    case '.vob':
      return 'Видео';
    case '.mp3':
    case '.aac':
    case '.flac':
    case '.alac':
    case '.ogg':
    case '.wav':
      return 'Аудио';
    case '.pdf':
    case '.xml':
    case '.doc':
    case '.docx':
    case '.txt':
    case '.odt':
      return 'Документ';
    case '.zip':
    case '.rar':
    case '.7z':
    case '.gz':
      return 'Архив';
    case '.exe':
      return 'Программа';
    case '.torrent':
      return 'Торрент';
    default:
      return (path.extname(filename)).replace('.','').toUpperCase() + ' файл';
  }
}