import Vue from 'vue'
import App from './App.vue'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueIcon from 'vue-icon'

Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.use(VueIcon, 'v-icon');

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
