import Vue from 'vue'
import Vuex from 'vuex'
import fileManager from './modules/fileManager'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    fileManager
  }
})
