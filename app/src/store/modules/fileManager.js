import axios from 'axios'

export default {
  state: {
    leftPane: {},
    rightPane: {}
  },
  mutations: {
    updateDirectory(state, { directory, side }) {
      switch (side) {
        case 'left':
          state.leftPane = directory;
          break;
        case 'right':
          state.rightPane = directory;
          break;
        default:
          state.leftPane = directory;
          state.rightPane = directory;
          break;
      }
    },
    updateSelected(state, { selected, side }) {
      switch (side) {
        case 'left':
          if (Array.isArray(selected) && selected)
            state.leftPane.selected = selected;
          if ( (state.leftPane.selected.length > 0) && (state.leftPane.selected[0] !== null) )
            state.leftPane.status = `Выбрано ${state.leftPane.selected.length} из ${state.leftPane.items.length}`;
          else
            state.leftPane.status = `Объекты: ${state.leftPane.items.length}`;
          state.leftPane.active = true;
          state.rightPane.active = false;
          break;
        case 'right':
          state.rightPane.selected = Array.isArray(selected) ? selected : [selected];
          if ( (state.rightPane.selected.length > 0) && (state.rightPane.selected[0] !== null) )
            state.rightPane.status = `Выбрано ${state.rightPane.selected.length} из ${state.rightPane.items.length}`;
          else
            state.rightPane.status = `Объекты: ${state.rightPane.items.length}`;
          state.rightPane.active = true;
          state.leftPane.active = false;
          break;
        default:
          break;
      }
    },
    updateActive(state, { side }) {
      switch (side) {
        case 'left':
          state.leftPane.active = true;
          state.rightPane.active = false;
          break;
        case 'right':
          state.rightPane.active = true;
          state.leftPane.active = false;
          break;
        default:
          break;
      }
    }
  },
  actions: {
    async fetchDirectory(ctx, { path, side }) {
      await axios
        .post("/api/folder", { path })
        .then(response => {
            ctx.commit('updateDirectory', { directory: response.data, side });
            ctx.commit('updateSelected', { selected: [], side });
          },
          (error) => {
            console.log(error);
          });
    },
    async copyItems(ctx, items) {
      let from = (ctx.state.leftPane.active) ? ctx.state.leftPane.path : ctx.state.rightPane.path;
      let to = (ctx.state.leftPane.active) ? ctx.state.rightPane.path : ctx.state.leftPane.path;
      if (from === to)
        console.log('Начальный и конечный путь совпадают');
      await axios
        .post('/api/copy', { items, from, to })
        .then(response => {
            console.log(response);
          },
          (error) => {
            console.log(error);
          });
    },
    async moveItems(ctx, items) {
      let from = (ctx.state.leftPane.active) ? ctx.state.leftPane.path : ctx.state.rightPane.path;
      let to = (ctx.state.leftPane.active) ? ctx.state.rightPane.path : ctx.state.leftPane.path;
      if (from === to)
        console.log('Начальный и конечный путь совпадают');
      await axios
        .post('/api/move', { items, from, to })
        .then(response => {
            console.log(response);
          },
          (error) => {
            console.log(error);
          });
    },
    async deleteItems(ctx, items) {
      await axios
        .post('/api/delete', { items })
        .then(response => {
            console.log(response);
          },
          (error) => {
            console.log(error);
          });
    }
  },
  getters: {
    pane(state) {
      return side => {
        switch (side) {
          case 'left':
            return state.leftPane;
          case 'right':
            return state.rightPane;
          default:
            return;
        }
      }
    }
  }
}